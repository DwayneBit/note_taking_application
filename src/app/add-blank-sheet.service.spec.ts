import { TestBed } from '@angular/core/testing';

import { AddBlankSheetService } from './add-blank-sheet.service';

describe('AddBlankSheetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddBlankSheetService = TestBed.get(AddBlankSheetService);
    expect(service).toBeTruthy();
  });
});
