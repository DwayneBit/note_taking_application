import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BlankSheetComponent } from './blank-sheet/blank-sheet.component';
import { StyleBarComponent } from './style-bar/style-bar.component';
import { AddBlankSheetService } from './add-blank-sheet.service';

@NgModule({
  declarations: [
    AppComponent,
    BlankSheetComponent,
    StyleBarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [AddBlankSheetService],
  bootstrap: [AppComponent],
  entryComponents: [BlankSheetComponent]
})
export class AppModule { }
