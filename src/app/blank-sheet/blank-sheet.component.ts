import { Component, Input, OnInit, Output, EventEmitter, HostListener, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-blank-sheet',
  templateUrl: './blank-sheet.component.html',
  styleUrls: ['./blank-sheet.component.css']
})
export class BlankSheetComponent  implements OnInit, AfterViewInit {

  constructor() {}
  
  ngOnInit(){
    console.log(this.pageNumber)
    this.sheetTextId = "sheet-text-"+this.pageNumber;
    this.blankSheetId = "blank-sheet-"+this.pageNumber;
  }    

  ngAfterViewInit(){

    if(this.pageNumber == 0){
      document.getElementById(this.blankSheetId).style.marginTop += "20px";
    }

    let sheetText = document.getElementById(this.sheetTextId);
    sheetText.addEventListener("scroll",
    (event:any)=>{
      if(!this.endOfPage){
        this.overFlow.emit();
        this.endOfPage = true;
      }
    },
    false
    ); 

  }
  
  @Input()
  pageNumber:number;

  sheetTextId:string;
  blankSheetId:string;

  @Output()
  overFlow = new EventEmitter()

  count:number=0;

  endOfPage:boolean = false;

  // @HostListener('document:overflow', ['$event'])
  // onOverflow(event:any){
  //   if(!this.endOfPage){
  //     this.overFlow.emit();
  //     this.endOfPage = true;
  //     console.log(1);
  //   }
  // }

  keyPress(e: any){
    let letters:RegExp = /[\w]+/
    let inputChar:string = e.key;
    if(e.ctrlKey)
      return
    if(inputChar=='Backspace' && this.count>0){
      this.count--;
      console.log(this.count );
    }
    else if(letters.test(inputChar) && inputChar.length==1)
    {  
      this.count++;
      console.log(this.count );
    }
    //console.log(e.key );
    // this.onOverFlow()
  }

  onPaste(e:any){
    let clipboardData = e.clipboardData;
    let pastedText = clipboardData.getData('text');
    this.count += (pastedText?pastedText.length:0);
    console.log(this.count );
    // this.onOverFlow()
  }

  onFocus(){
    let sheetText = document.getElementById(this.sheetTextId);
    this.count = (sheetText.textContent?sheetText.textContent.length:0);
    //console.log(this.count );
    // this.onOverFlow()
  }

  // onOverFlow(){
  //   let sheetText = document.getElementById('sheet-text');
  //   let lineHeight=Number.parseInt(window.getComputedStyle(sheetText).lineHeight);
  //   // console.log(sheetText.offsetHeight);
  //   // console.log(lineHeight);
  //   if(sheetText.offsetHeight==900){
  //     this.overFlow.emit()
  //     //this.absService.addBlankSheetComponent(AppComponent.pageNumber++)
  //   }
  // }

  /*******************ngStyles************************ */
  blankSheetStyle ={

    "height": "1000px",
    "margin-bottom": "10px",

    "background-color": "white",
    "outline": "none",
    "overflow": "auto"

  }

  sheetTextStyle = {
    'margin-top': '60px',
    'margin-bottom': '40px',
    'margin-right': '20px',
    'margin-left': '20px',
    'max-height': '900px',

    'display': 'block',
    'line-height': '20px',

    'overflow': 'hidden',
    'outline': 'none',
    
    'border':'1px solid black' /* for testing and visualization */
  }


}