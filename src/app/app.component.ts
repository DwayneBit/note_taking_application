import { Component, Input, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { AddBlankSheetService } from './add-blank-sheet.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  show=false;
  @ViewChild('dynamic', { 
    read: ViewContainerRef 
  }) viewContainerRef: ViewContainerRef

  constructor(private absService:AddBlankSheetService) {
    
  };

  ngOnInit(){
    this.absService.setRootViewContainerRef(this.viewContainerRef)
    this.absService.addBlankSheetComponent()
  }

  showNav() {
    this.show = this.show ? false : true;
  }

}
