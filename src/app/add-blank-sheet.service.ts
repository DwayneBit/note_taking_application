
import {
  ComponentFactoryResolver,
  Injectable,
  ViewContainerRef
} from '@angular/core'

import { BlankSheetComponent } from './blank-sheet/blank-sheet.component'

@Injectable({
  providedIn: 'root'
})

export class AddBlankSheetService {

  static pageNumber:number = 0;

  constructor( private factoryResolver:ComponentFactoryResolver) {
    this.factoryResolver = factoryResolver
  }

  rootViewContainer:ViewContainerRef
  setRootViewContainerRef(viewContainerRef:ViewContainerRef) {
    this.rootViewContainer = viewContainerRef
  }

  addBlankSheetComponent() {
    const componentFactory = this.factoryResolver
                        .resolveComponentFactory(BlankSheetComponent);
    const component = componentFactory
      .create(this.rootViewContainer.parentInjector);
    component.instance.overFlow.subscribe( $event => {
      this.addBlankSheetComponent()
    })
    component.instance.pageNumber = AddBlankSheetService.pageNumber++; //insert the page number into new component
    this.rootViewContainer.insert(component.hostView);
    
  }
}