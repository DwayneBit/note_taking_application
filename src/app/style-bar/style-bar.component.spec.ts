import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleBarComponent } from './style-bar.component';

describe('StyleBarComponent', () => {
  let component: StyleBarComponent;
  let fixture: ComponentFixture<StyleBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
