import { Component, OnInit, Input } from '@angular/core';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-style-bar',
  templateUrl: './style-bar.component.html',
  styleUrls: ['./style-bar.component.css']
})
export class StyleBarComponent{

  constructor() { }

  NUMS:string[] = ['2','4','6','8','10','12','14','16','18','20','22','24']

  mkBold(){
    document.execCommand("bold");
  }

  mkItalic(){
    document.execCommand("italic");
  }

  mkHighlight(){
    if (!document.execCommand("HiliteColor", false, "yellow")) {
      document.execCommand("BackColor", false, "yellow");
    }
  }

  mkBig(selectedValue:string){
    let size = document.createElement("span") // create element to surrond selected text in
    size.style.fontSize = selectedValue+'px';
    let range = document.getSelection().getRangeAt(0)
   /* range.surroundContents(size); get range of selected text
    and surrond it in element size*/
    console.log(range.startOffset+" "+ range.endOffset)
  }

}